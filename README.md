# Task API with Node.js and Express - TypeScript settings 

## Install TypeScript globally (tsc compiler installation)
```
npm i -g typescript
```

## TypeScript to Javascript automatic transpiling (Watch mode)
```
tsc app.ts -w
```

## Create TypeScript config file (tsconfig.json file) ("target": "es6", "outDir": "./dist", "rootDir": "./src", "moduleResolution": "node",)
```
tsc --init
```

## Initialize dependencies file with default settings (package.json file)
```
npm init -y
```

## Install express
```
npm i express
```

## Install development dependencies
```
npm i -D typescript ts-node nodemon @types/node @types/express @types/uuid
```

## Install uuid and uuid type
```
npm i uuid
```

## Install uuid type
```
npm i -D @types/uuid
```

## Start the server on dev mode (nodemon watching changes made on src/app.ts)
```
npm run dev
```
